#pragma comment(lig, "Ws2_32.lib")

#include <iostream>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <windows.h>
#include "mainwindow.h"
#include "ui_mainwindow.h"

// Server info
const char *serverIP;
// Buffer
char sendbuf[256];
char receivebuf[256];
//Server data
int iRes;
WSAData wsaData;
SOCKET sConnect;
sockaddr_in conpar;
int conparlen;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // ws2_32.dll aktivieren
    iRes = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iRes == 0) {
        ui->textBrowser->append("WSAStartup() \t\t successful");
    }
    else {
        ui->textBrowser->append("error WSAStartup()");
    }
    sConnect = socket(AF_INET, SOCK_STREAM, 0);
    if (sConnect != INVALID_SOCKET) {
        ui->textBrowser->append("socket() \t\t successful");
    }
    else {
        ui->textBrowser->append("error socket()");
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connectButton_clicked()
{
    serverIP = ui->serverIPEdit->text().toStdString().c_str();
    conpar.sin_addr.s_addr = inet_addr(serverIP);
    conpar.sin_family      = AF_INET;
    conpar.sin_port        = htons(54345);
    conparlen = sizeof(conpar);

    iRes = ::connect(sConnect, (struct sockaddr*)&conpar, conparlen);
    if (iRes != SOCKET_ERROR) {
        ui->textBrowser->append("connect() \t\t successful");
    }
    else {
        ui->textBrowser->append("connected() \t\terror");
    }
    memset(&sendbuf, 0, sizeof(sendbuf));
        memset(&receivebuf, 0, sizeof(receivebuf));

        iRes = recv(sConnect, receivebuf, 256, 0);

        if(iRes == SOCKET_ERROR) {
            ui->textBrowser->append("recv(): \t\terror");
        }
        else {
            ui->textBrowser->append(receivebuf);
        }

}
